﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReclamaPOA.Models
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }

        public List<Reclamacao> Reclamacoes { get; set; }

        //public override string ToString()
        //{
        //    return String.Format("{0}: {1} - {2}", CategoriaId, Nome, Descricao);
        //}
    }
}