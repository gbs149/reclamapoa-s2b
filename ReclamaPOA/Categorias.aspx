﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Categorias.aspx.cs" Inherits="ReclamaPOA.Categorias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul>
        <asp:ListView
            ID="ListaCategorias"
            ItemType="ReclamaPOA.Models.Categoria"
            runat="server"
            SelectMethod="getCategorias">
            <ItemTemplate>
                <ul>
                    <%#:Item.Nome %>
                </ul>
            </ItemTemplate>
        </asp:ListView>
    </ul>
</asp:Content>
