﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReclamaPOA.Startup))]
namespace ReclamaPOA
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
