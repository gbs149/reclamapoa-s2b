﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NovaReclamacao.aspx.cs" Inherits="ReclamaPOA.NovaReclamacao" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Label ID="Label1" AssociatedControlID="txtNomeRec" runat="server" Text="Label">Título da Reclamação</asp:Label>
    <asp:TextBox ID="txtNomeRec" runat="server"></asp:TextBox>
    <br />

    <asp:Label ID="Label2" AssociatedControlID="txtDescRec" runat="server" Text="Label">Descrição</asp:Label>
    <asp:TextBox ID="txtDescRec" runat="server"></asp:TextBox>


    <asp:Label ID="Label3" AssociatedControlID="ddlBairros" runat="server" Text="Label">Bairro</asp:Label>
    <asp:DropDownList ID="ddlBairros" runat="server" AutoPostBack="True"></asp:DropDownList>
    
    <asp:Label ID="Label4" AssociatedControlID="txtEndereco" runat="server" Text="Label">Endereço</asp:Label>
    <asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox>

    <asp:Label ID="Label6" AssociatedControlID="txtUrlImagem" runat="server" Text="Label">Link para imagem</asp:Label>
    <asp:TextBox ID="txtUrlImagem" runat="server"></asp:TextBox>

    <%--TODO Validação em categorias --%>
    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
    <asp:Label ID="Label5" AssociatedControlID="rblCategorias" runat="server" Text="Label">Categoria</asp:Label>
    <asp:RadioButtonList ID="rblCategorias" runat="server"></asp:RadioButtonList>

    <asp:Button ID="btnSalvar" runat="server" Text="Registrar reclamação" OnClick="btnSalvar_Click" />


    <%--</EditItemTemplate>
    </asp:FormView>--%>
</asp:Content>
